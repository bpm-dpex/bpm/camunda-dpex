import * as React from 'react';
import { Button, Grid } from '@mui/material';
import ButtonGroup from '../ButtonGroup';
import AdapterCreationConfig from '../adapterTable/AdapterCreationConfig'
import Utility from '../utils/Utility';

const BPMAdapterCreationDiv = ({setAdapters, closeDialog}) => {
    const [adapterTypes, setAdapterTypes] = React.useState([]);
    const [error_status, setErrorStatus] = React.useState([]);
    const [chosen_adapter, setChosenAdapter] = React.useState();

    React.useEffect(() => {
      getBPMAdapterTypes(setAdapterTypes)}, [])

    var handleToClose = () => {
      closeDialog();
    };
  
    return (<>
        <AdapterCreationConfig adapterTypes={adapterTypes} setChosenAdapter={setChosenAdapter} handleSelection={handleSelection}/>
        <ButtonGroup>
            <Button variant="outlined" onClick={() => letsCreate(chosen_adapter, setErrorStatus, setAdapters)}>
                Create
            </Button>      
            <span id="deploy_msg"></span>
        </ButtonGroup>
        <span>{error_status}</span>
      </>
    );
 }

 const handleSelection = function(event, setter){
  setter(event.target.value);
}

function getBPMAdapterTypes(setAdapterTypes){
    const url = "http://localhost:8086/api/bpmEngineTypes";
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                setAdapterTypes(JSON.parse(request.responseText));
            }else{
            }
        }
    };
    request.send();
}

/**
 * 
 * @param {*} adapterId the name of the adapter to create
 * @param {*} setErrorStatus 
 */
function letsCreate(adapterId, setErrorStatus, setAdapters){
      const url = "http://localhost:8086/api/bpmEngine";
      const request = new XMLHttpRequest();

      var creationDTO = {
        type: adapterId,
        name: document.getElementById("adapter_creation_name").value,
        connection: document.getElementById("adapter_creation_connection").value
      }


      request.open("POST", url, true);
      request.setRequestHeader("Content-Type", "application/json");
      request.onreadystatechange = () => {
          if (request.readyState == 4) {
              if(request.status < 300 && request.status > 199){
                  setErrorStatus("Success!");
                  Utility.refreshBpmAdapters(setAdapters)
              }else{
                  setErrorStatus("Error: " + request.status);
              }
          }
      };
      request.send(JSON.stringify(creationDTO));
}

 export default BPMAdapterCreationDiv;