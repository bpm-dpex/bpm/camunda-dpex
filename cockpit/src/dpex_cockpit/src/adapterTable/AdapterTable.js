import React, { useState, useEffect } from "react";
import {Table, TableContainer, TableBody, TableHead, TableCell, TableRow} from "@mui/material";

function AdapterTable({adapters, attributes, api_attributes}) {
    if(adapters.length == 0){
        return(<>
            There are no adapters yet<br></br>
        </>)
    }
    return (
        <TableContainer>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow> 
                        {
                            CreateAdapterTableHeader(attributes)
                        }
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                        CreateAdapterTableRows(api_attributes, adapters)
                    }
                </TableBody>
            </Table>
        </TableContainer>
    )
}

const CreateAdapterTableRows = function (attributes, adapters){
    var adapter_rows = [];
    for(var i = 0; i < adapters.length; i++){   
        adapter_rows.push(
            <TableRow>
                {
                    CreateAdapterTableRowColumns(attributes, adapters[i])
                }
            </TableRow>);  
    }    
    return adapter_rows;
}

const CreateAdapterTableRowColumns = function(attributes, adapter){
    var adapter_rows = [];
    adapter_rows.push(
        <TableCell align="left" width={"33%"}  style={{fontSize: "12px"}}>{adapter[attributes[0]]}</TableCell>
    );
    for(var i = 1; i < attributes.length; i++){   
        adapter_rows.push(
            <TableCell align="right" width={"33%"}  style={{fontSize: "12px"}}>{adapter[attributes[i]]}</TableCell>
        );
    }    
    return adapter_rows;
}

const CreateAdapterTableHeader = function (attributes){
    var adapter_header = [];
    adapter_header.push(
        <TableCell align="left" width={"33%"} style={{fontSize:"12px"}}>{attributes[0]}</TableCell>
    );
    for(var i = 1; i < attributes.length; i++){   
        adapter_header.push(
            <TableCell align="right" width={"33%"} style={{fontSize:"12px"}}>{attributes[i]}</TableCell>
        );
    }    
    return adapter_header;
}

export default AdapterTable;
