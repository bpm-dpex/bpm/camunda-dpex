import React, { useState, useEffect } from "react";
import { NativeSelect } from "@mui/material";

var base_url = "http://localhost:8086/api/"

function refreshAlliances(bpmId, setAlliances){
    console.log("bpmId: " + bpmId);
    const url = "http://localhost:8086/api/alliances?bpmId=" + bpmId;
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if (request.status > 199 && request.status < 300) {
                setAlliances(JSON.parse(request.responseText));
            } 
        }
    };
    request.send();
}


function refreshBpmAdapters(set){
    const url = base_url + "bpmEngines";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                set(adapters);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

function refreshCollAdapters(set){
    const url = base_url + "collaborations";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                set(adapters);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

function refreshCommAdapters(set){
    const url = base_url + "communicationModules";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                set(adapters);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send();
}

const refreshUsers = (setUsers) => {
    const url = "http://localhost:8086/api/allUsers";
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if(request.readyState == 4){
            if (request.status > 199 && request.status < 300) {
                setUsers(JSON.parse(request.responseText));
            } 
        }
    };
    request.send();
}

export default {refreshBpmAdapters, refreshCollAdapters, refreshCommAdapters, refreshAlliances, refreshUsers}