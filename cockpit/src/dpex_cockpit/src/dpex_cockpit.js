import React, {useState} from "react";
import ReactDOM from "react-dom";

import DpexCockpit from "./DpexCockpit";

let container;

export default {
    id: 'dpex.cockpit.dashboard',
    pluginPoint: 'cockpit.dashboard',
    priority: 9001,
    render: (node, {api}, task) => {
      container = node;
      window.my_camunda_api = api;
      window.my_camunda_task = task;
      ReactDOM.render(
        <DpexCockpit/>,
        container
      );
    },
  }