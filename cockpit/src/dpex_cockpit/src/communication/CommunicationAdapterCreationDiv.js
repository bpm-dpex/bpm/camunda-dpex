import React, { useState, useEffect } from "react";
import { Button, Grid } from '@mui/material';
import ButtonGroup from '../ButtonGroup';
import AdapterCreationConfig from '../adapterTable/AdapterCreationConfig'
import Utility from "../utils/Utility";

const CommunicationAdapterCreationDiv = ({closeDialog, setAdapters}) => {
    const [adapterTypes, setAdapterTypes] = React.useState([]);
    const [error_status, setErrorStatus] = React.useState([]);
    const [chosen_adapter, setChosenAdapter] = React.useState();

    React.useEffect(() => {
        getCommAdapterTypes(setAdapterTypes)}, [])

    var handleToClose = () => {
      closeDialog();
    };
  
    return (<>
      <AdapterCreationConfig adapterTypes={adapterTypes} setChosenAdapter={setChosenAdapter} handleSelection={handleSelection}/>
      <ButtonGroup>
        <Button variant="outlined" onClick={() => letsCreate(chosen_adapter, setErrorStatus, setAdapters)}>
            Create
        </Button>      
        <span id="deploy_msg"></span>
      </ButtonGroup>
      <span>{error_status}</span>
      </>
    );
 }

 const handleSelection = function(event, setter){
  setter(event.target.value);
}

function getCommAdapterTypes(setAdapterTypes){
    const url = "http://localhost:8086/api/communicationModuleTypes";
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                var adapters = JSON.parse(request.responseText);
                setAdapterTypes(adapters);
            }else{
            }
        }
    };
    request.send();
}

function letsCreate(adapterId, setErrorStatus, setAdapters){
      const url = "http://localhost:8086/api/communicationModule";
      const request = new XMLHttpRequest();

      var creationDTO = {
        type: adapterId,
        name: document.getElementById("adapter_creation_name").value,
        connection: document.getElementById("adapter_creation_connection").value
      }

      request.open("POST", url, true);
      request.setRequestHeader("Content-Type", "application/json");
      request.onreadystatechange = () => {
          if (request.readyState == 4) {
              if(request.status < 300 && request.status > 199){
                    setErrorStatus("Success!");
                    Utility.refreshCommAdapters(setAdapters);
                }else{
                  setErrorStatus("Error: " + request.status);
              }
          }
      };
      request.send(JSON.stringify(creationDTO));
  }

 export default CommunicationAdapterCreationDiv;