import React, { useState } from "react";
import {Dialog, DialogTitle, DialogContent, Button} from "@mui/material";
import CollaborationAdapterCreationDiv from "./CollaborationAdapterCreationDiv";
import CollaborationAdapterDeletionDiv from "./CollaborationAdapterDeletionDiv";

const CollaborationAdapterButtons = ({adapters, setAdapters}) => {
    const [open1, setOpen1] = React.useState(false);
    const [open2, setOpen2] = React.useState(false);

    const handleClickToOpen1 = () => {
        setOpen1(true);
    };
 
    const handleToClose1 = () => {
        setOpen1(false);
    };

    const handleClickToOpen2 = () => {
        setOpen2(true);
    };
 
    const handleToClose2 = () => {
        setOpen2(false);
    };

    return (<>
    <Button variant="outlined" onClick={handleClickToOpen1} sx={{ p: 2 }} style={{
            maxWidth: "5em",
            maxHeight: "3em",
            paddingTop: "2em"
        }}>Create Adapters</Button>
        <Dialog open={open1} onClose={handleToClose1} style={{minWidth: "50em"}}>
            <DialogTitle><h4>Create an adapter</h4></DialogTitle>
            <DialogContent>
                <CollaborationAdapterCreationDiv closeDialog={setOpen1} setAdapters={setAdapters}/>
            </DialogContent>
        </Dialog>
    <Button variant="outlined" onClick={handleClickToOpen2} sx={{ p: 2 }} style={{
            maxWidth: "5em",
            maxHeight: "3em",
            paddingTop: "2em",
            marginLeft: '.5rem'
        }}>Delete Adapters</Button>
        <Dialog open={open2} onClose={handleToClose2} style={{minWidth: "50em"}}>
            <DialogTitle><h4>Delete adapters</h4></DialogTitle>
            <DialogContent>
                <CollaborationAdapterDeletionDiv closeDialog={setOpen2} setAdapters={setAdapters} adapters={adapters}/>
            </DialogContent>
        </Dialog>
    </>);
 }

 export default CollaborationAdapterButtons;