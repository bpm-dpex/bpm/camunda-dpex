import React, { useState, useEffect } from "react";
import { Dialog, Button, DialogContent, DialogTitle, Grid, NativeSelect} from "@mui/material";
import styled from 'styled-components';
import Utility from "../utils/Utility";

const DeploySite = ({bpm_adapters, communication_adapters, collaboration_adapters, setAlliances, bpmId, setUsers}) => {
    const [open, setOpen] = React.useState(false);
    const [error_status, setErrorStatus] = React.useState("");
    const [current_collaboration, setCollaboration] = React.useState("");
    const [current_communication, setCommunication] = React.useState("");
    const [current_bpm, setBPM] = React.useState("");
 
    const handleClickToOpen = () => {
        setOpen(true);
    };
 
    const handleToClose = () => {
        setOpen(false);
    };
 
    //const orgModel = 'BpmId,SciId,CommunicationId,HomeserverUrl,Roles,Departments\nGE,0xE076df4e49182f0AB6f4B98219F721Cccc38f9be,@ge_:matrix.org,https://matrix.org,Einkauf,Scheinwerferhersteller\nOX,0x4B1184629DE85ab53cF86477D190a9f3740ABdF5,@ox_:matrix.org,https://matrix.org,Produktion,Scheinwerferhersteller\nGU,0x33d329263B56742607E2710a7CC5D927F1F279d3,@g.u_:matrix.org,https://matrix.org,Produktion,Scheinwerferhersteller\nVS,0x862C252D5e3fd90436CfC17d41169a0B85872471,@v.s_:matrix.org,https://matrix.org,Qualitätsmanagement,Scheinwerferhersteller\nBM,0x422039b408CeCb32361F9D5EF3E2Eb5c0b0d0Cc2,@b.m_:matrix.org,https://matrix.org,Produktion,LED-Hersteller\nNV,0x21aF08CDAC443f2f8de351c515ABbe3bB5DB9b90,@nv_:matrix.org,https://matrix.org,Produktion,LED-Hersteller\nCN,0x7aFCd0B490bc1a5b142EBC455ef879F872ea8AFa,@c.n_:matrix.org,https://matrix.org,Qualitätsmanagement,LED-Hersteller\nIU,0xbc1535f4D9Ee27b79964fDC47E986DcfA4d8A64c,@iu_:matrix.org,https://matrix.org,Vertrieb,LED-Hersteller';

    return (<>
    <Button variant="outlined" onClick={handleClickToOpen} sx={{ p: 2 }} style={{
          maxWidth: "5em",
          maxHeight: "3em"
        }}>dpex-deploy</Button>
        <Dialog open={open} onClose={handleToClose}>
            <DialogTitle><h4>Deploy new processes</h4></DialogTitle>
            <DialogContent>
                <Grid container direction="column" spacing={2} paddingTop={1.5}>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Process Model:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="process_model_file_input" type="file" accept=".bpmn" onChange={() => handleModelFile()}/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Scripts:
                        </Grid>
                        <Grid item xs={7}> 
                        <input id="script_file_input" type="file" accept=".py" multiple="multiple"/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}>  
                            Organizational Model:
                        </Grid>
                        <Grid item xs={7}>  
                            <input id="org_model_file_input" type="file" accept=".org"/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Process ID:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="process_model" readOnly={true}/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Sci Key:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="sciAuthKey" defaultValue={"Basic ZTB5djFueXA2aTptQU05ZmNyVUhTdjVySWZfTkFKNUlqMW9xcGtkVHFwbEdtd0xkWlpSUjF3"}/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Sci ID:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="sciId" defaultValue={"0xE076df4e49182f0AB6f4B98219F721Cccc38f9be"}/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Alliance Name:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="alliance_name" defaultValue={"my_alliance"}/>
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            BPM Engine:
                        </Grid>
                        <Grid item xs={7}> 
                            {createAdapterSelection(setBPM, bpm_adapters)}
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Collaboration:
                        </Grid>
                        <Grid item xs={7}> 
                            {createAdapterSelection(setCollaboration, collaboration_adapters)}
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Communication:
                        </Grid>
                        <Grid item xs={7}> 
                            {createAdapterSelection(setCommunication, communication_adapters)}
                        </Grid>
                    </Grid>
                    <Grid container direction="row" spacing={1} padding={1}>
                        <Grid item xs={5}> 
                            Existing GAR:
                        </Grid>
                        <Grid item xs={7}> 
                            <input id="gar"/>
                        </Grid>
                    </Grid>
                </Grid>
                <ButtonGroup>
                    <Button variant="outlined" onClick={() => deploy(setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers)}>
                        Deploy
                    </Button>      
                </ButtonGroup>
                <span>{error_status}</span>
            </DialogContent>
        </Dialog>
    </>);
}

export default DeploySite;

const handleModelFile = async () => {
    var input = document.getElementById("process_model_file_input");
    if(input == null) return;
    var file = input.files[0];
    var text = await file.text();

    const xmlStr = text;
    const parser = new DOMParser();
    const doc = parser.parseFromString(xmlStr, "application/xml");
    var result = ""
    var bpmn_process = doc.getElementsByTagName("bpmn:process")[0];
    if(bpmn_process){
        result = bpmn_process.getAttribute("id");
    }else{
        result = "no ID found";
    }
    document.getElementById("process_model").value = result;
}

function deploy(setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers){
    var gar = document.getElementById("gar").value;
    var name = document.getElementById("alliance_name").value;
    var process_model_file = document.getElementById("process_model_file_input").files[0];
    var org_model_file = document.getElementById("org_model_file_input").files[0];

    if(process_model_file == undefined){
        setErrorStatus("No process model was specified");
        return;
    }
    if(org_model_file == undefined){
        setErrorStatus("No org. model was specified");
        return;
    }
    setErrorStatus("Loading...")
    const url = "http://localhost:8086/api/allianceNameInUse?name=" +  name;
    
    const request = new XMLHttpRequest();

    request.open("GET", url, true);
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                if(request.responseText === "false"){
                    if(gar === ""){
                        deployContract(setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers);
                    }else{
                        setGar(gar);
                        createModel(gar,setErrorStatus, current_bpm, current_collaboration, current_communication, setUsers);
                    }                }else{
                    setErrorStatus("That name is already in use and should not be used!");
                }
            }else{
                setErrorStatus("Error: " + request.status);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify());
}

function uploadScripts(gar){
    var files = document.getElementById("script_file_input").files;
    for(var f of files){
        uploadScript(gar, f)
    }
}

function uploadScript(gar, file){
    const url = `http://localhost:8086/api/` + gar + "/script" ;
    const request = new XMLHttpRequest();
    const formData = new FormData();

    request.open("POST", url, true);
    request.onreadystatechange = () => {
        //Nothing to do here
    };
    formData.append("scriptFile", file);
    request.send(formData);
}

function deployContract(setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers){
    var connection = current_collaboration.split(";;")[1];
    var sciId = document.getElementById("sciId").value;
    var sciAuthKey = document.getElementById("sciAuthKey").value;

    var body = {
        'connection': connection,
        'sciId': sciId,
        'apiKey': sciAuthKey
    }

    const url = `http://localhost:8086/api/ethereum/deploySCI`;
    
    const request = new XMLHttpRequest();

    request.open("POST", url, true);
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            var created_gar = request.responseText;
            if(request.status < 300 && request.status > 199){
                createModel(created_gar, setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers);
            }else{
                setErrorStatus("Error: " + request.status);
            }
        }
    };
    request.setRequestHeader("Content-Type", "application/json");
    request.send(JSON.stringify(body));
}

async function createModel(gar, setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId, setUsers){
    var process_model_file = document.getElementById("process_model_file_input").files[0];
    var org_model_file = document.getElementById("org_model_file_input").files[0];
    var org_model = await org_model_file.text();
    const url = "http://localhost:8086/api/model";
    
    const request = new XMLHttpRequest();
    const formData = new FormData();

    request.open("POST", url, true);
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                createAlliance(gar, setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId);
                Utility.refreshUsers(setUsers);
            }else{
                setErrorStatus("Error: " + request.status);
            }
        }
    };

    formData.append("organizationalModel", org_model);
    formData.append("processModel", process_model_file);
    request.send(formData);
}

function createAlliance(created_gar, setErrorStatus, current_bpm, current_collaboration, current_communication, setAlliances, bpmId){
    var name = document.getElementById("alliance_name").value;
    var process_model = document.getElementById("process_model").value;
    var collaboration = current_collaboration;
    var bpm_engine = current_bpm;
    var communication = current_communication;

    const url = "http://localhost:8086/api/alliance";
    
    const request = new XMLHttpRequest();
    const formData = new FormData();

    request.open("POST", url, true);
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                setErrorStatus("Success!");
                uploadScripts(created_gar);
                Utility.refreshAlliances(bpmId, setAlliances);
            }else{
                setErrorStatus("Error: " + request.status);
            }
        }
    }
    formData.append("name", name);
    formData.append("GAR", created_gar);
    formData.append("processModel", process_model);
    formData.append("organizationalModel", "processModel");
    formData.append("bpmEngine", bpm_engine.split(";;")[0]);
    formData.append("collaboration", collaboration.split(";;")[0]);
    if(communication != ""){
        formData.append("communication", communication.split(";;")[0]);
    }
    request.send(formData);
}

const createKnownAdapterElements = function (adapters){
    let adapter_items = [];
    adapter_items.push(<option value={""}>{"--"}</option>);  
    for(var a of adapters){
        adapter_items.push(<option value={a.name + ";;" + a.connection}>{a.name}</option>);  
    }    
    return adapter_items;
}

const createAdapterSelection = function (set, adapters){
    let selection = [];
    selection.push(
        <NativeSelect style={{minWidth:"70%"}} onChange={(event) => {set(event.target.value);}}>
            {createKnownAdapterElements(adapters)}
        </NativeSelect>
    )
    return selection;
}

const ButtonGroup = styled.div`
  display: flex;
`