import React, { useState } from "react";
import AllianceButtons from "./AllianceButtons";
import AllianceTable from "./AllianceTable";
import UserSelection from "./user_selection/UserSelection";

function Alliances({alliances, setAlliances, setBpmId, users, bpmId}){
    return(<>
        <h3>Alliances of user <UserSelection users={users} setAlliances={setAlliances} setBpmId={setBpmId}/>:</h3>
        <AllianceTable alliances={alliances}/>
        <AllianceButtons alliances={alliances} setAlliances={setAlliances} bpmId={bpmId}/>
    </>
    );
}

export default Alliances;
