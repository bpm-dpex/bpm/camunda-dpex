import React from "react";
import Utility from "../../utils/Utility";
import {NativeSelect} from "@mui/material";

const UserSelection = ({users, setAlliances, setBpmId}) => {
    return(<>
        <NativeSelect onChange={(event) => handleUserSelection(event, setAlliances, setBpmId)} style={{fontSize:"20px"}}>
            {createUserElements(users)}
        </NativeSelect>
    </>)
}

const handleUserSelection = (event, setAlliances, setBpmId) => {
    var bpmId = event.target.value;
    setBpmId(bpmId);
    Utility.refreshAlliances(bpmId, setAlliances)
}

const createUserElements = (users) => {
    let alliance_items = [];
    alliance_items.push(<option value={""} style={{fontSize:"20px"}}>{"--"}</option>);  
    for(var u of users){   
        alliance_items.push(<option value={u["bpmId"]} style={{fontSize:"20px"}}>{u["bpmId"]}</option>);  
    }    
    return alliance_items;
}

export default UserSelection;