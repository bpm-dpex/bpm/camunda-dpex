import React from "react";
import Alliances from "./alliances/Alliances";
import CommunicationAdapters from "./communication/CommunicationAdapters";
import CollaborationAdapters from "./collaboration/CollaborationAdapters";
import BPMAdapters from "./bpm/BPMAdapters";
import DeploySite from "./deploy/DeploySite";
import Utility from "./utils/Utility";

function DpexCockpit(){
    const [bpmAdapters, setBpmAdapters] = React.useState([])
    const [commAdapters, setCommAdapters] = React.useState([]);
    const [collAdapters, setCollAdapters] = React.useState([]);
    const [alliances, setAlliances] = React.useState([]);
    const [bpmId, setBpmId] = React.useState("");
    const [users, setUsers] = React.useState([]);

    React.useEffect(() => {
        Utility.refreshCollAdapters(setCollAdapters);
        Utility.refreshCommAdapters(setCommAdapters);
        Utility.refreshBpmAdapters(setBpmAdapters);
        Utility.refreshUsers(setUsers);
    }, [])

    return(<>
        <DeploySite bpm_adapters={bpmAdapters} collaboration_adapters={collAdapters} 
            communication_adapters={commAdapters} setAlliances={setAlliances} bpmId={bpmId} setUsers={setUsers}/>
        <Alliances users={users} bpmId={bpmId} setBpmId={setBpmId} alliances={alliances} setAlliances={setAlliances}/>
        <BPMAdapters bpmAdapters={bpmAdapters} setBpmAdapters={setBpmAdapters}/>
        <CollaborationAdapters collAdapters={collAdapters} setCollAdapters={setCollAdapters}/> 
        <CommunicationAdapters commAdapters={commAdapters} setCommAdapters={setCommAdapters}/>
    </>
    );
}


export default DpexCockpit;
