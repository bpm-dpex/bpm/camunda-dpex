# Specify the version
FROM camunda/camunda-bpm-platform:tomcat-7.19.0

# Create directories for plugin files
RUN mkdir -p /camunda/webapps/camunda/app/cockpit/scripts/ba_camunda_dpex_plugin
RUN mkdir -p /camunda/webapps/camunda/app/tasklist/scripts/ba_camunda_dpex_plugin

# Copy config files
COPY cockpit/config.js /camunda/webapps/camunda/app/cockpit/scripts/config.js
COPY tasklist/config.js /camunda/webapps/camunda/app/tasklist/scripts/config.js

# Copy plugin files
COPY cockpit/src/dpex_cockpit/dist/plugin.js /camunda/webapps/camunda/app/cockpit/scripts/ba_camunda_dpex_plugin/dpex_cockpit_plugin.js
COPY tasklist/src/dpex_details/dist/plugin.js /camunda/webapps/camunda/app/tasklist/scripts/ba_camunda_dpex_plugin/dpex_details_plugin.js
COPY tasklist/src/setup/dist/plugin.js /camunda/webapps/camunda/app/tasklist/scripts/ba_camunda_dpex_plugin/dpex_setup_plugin.js

# Copy web.xml
COPY web.xml /camunda/webapps/camunda/WEB-INF/web.xml