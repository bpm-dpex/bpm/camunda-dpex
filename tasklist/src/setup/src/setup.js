import React from "react";
import ReactDOM from "react-dom";

import SetupComponents from "./SetupComponents";

let container;

export default {
    id: 'tasklist.dpex',
    pluginPoint: 'tasklist.navbar.action',
    priority: 9001,
    render: (node, {api}, task) => {
      container = node;
      window.my_camunda_api = api;
      window.my_camunda_task = task;
      ReactDOM.render(
        <SetupComponents/>,
        container
      );
    },
  }