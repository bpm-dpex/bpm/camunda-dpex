import React, { useState, useEffect } from "react";
import {Table, TableContainer, TableRow, TableCell, TableHead} from "@mui/material";
import { Dialog, Button, DialogContent, DialogTitle, Input, Grid} from "@mui/material";

function Variables({variables, setVariables}){
    const [openAdd, setOpenAdd] = React.useState(false);
    const [openRemove, setOpenRemove] = React.useState(false);

    const handleClickToOpenAdd = () => {
        setOpenAdd(true);
    };

    const handleToCloseAdd = () => {
        setOpenAdd(false);
    };

    const handleClickToOpenRemove = () => {
        setOpenRemove(true);
    };

    const handleToCloseRemove = () => {
        setOpenRemove(false);
    };

    if(Object.keys(variables).length == 0){
        return(<>
        No variables defined!<br></br>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <Button variant="outlined" onClick={handleClickToOpenAdd} style={{padding:"8px"}}>
                    Add Variable
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={handleClickToOpenRemove} style={{padding:"8px"}}>
                    Remove Variable
                </Button>
            </Grid>
        </Grid>

        <Dialog open={openAdd} onClose={handleToCloseAdd}>
        <DialogTitle>
            <h4>Add variables to the instantiation</h4>
        </DialogTitle>
        <DialogContent>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <span>Name:</span><br></br>
                <input id="new_value_name"></input><br></br>
            </Grid>
            <Grid item>
                <span>Value:</span><br></br>
                <input id="new_value_value"></input>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={() => addVariable(variables, setVariables)} style={{padding:"8px", height:"100%"}}>
                    Add
                </Button>
            </Grid>
        </Grid>
        <TableContainer>
            <Table sx={{ minWidth: 400 }} aria-label="simple table">
                <VariableList variables={variables}/>
            </Table>
        </TableContainer>
        </DialogContent>
        </Dialog>

        <Dialog open={openRemove} onClose={handleToCloseRemove}>
        <DialogTitle>
            {"remove variable"}
        </DialogTitle>
        <DialogContent>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <span style={{fontSize: "12px"}}>Name: </span><br></br>
                <input id="variable_to_remove_name"></input> <br></br>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={() => removeVariable(variables, setVariables)} style={{padding:"8px", height:"100%"}}>
                    Remove
                </Button>
            </Grid>
        </Grid>
        <TableContainer>
            <Table sx={{ minWidth: 400 }} aria-label="simple table">
                <VariableList variables={variables}/>
            </Table>
        </TableContainer>
        </DialogContent>
        </Dialog>
        </> 
        )
    }

    return(<>
        <TableContainer style={{minWidth:"40%"}}>
            <Table aria-label="simple table">
                <VariableList variables={variables}/>
            </Table>
        </TableContainer>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <Button variant="outlined" onClick={handleClickToOpenAdd} style={{padding:"8px"}}>
                    Add Variable
                </Button>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={handleClickToOpenRemove} style={{padding:"8px"}}>
                    Remove Variable
                </Button>
            </Grid>
        </Grid>

        <Dialog open={openAdd} onClose={handleToCloseAdd}>
        <DialogTitle>
            <h4>Add variables to the instantiation</h4>
        </DialogTitle>
        <DialogContent>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <span>Name:</span><br></br>
                <input id="new_value_name"></input><br></br>
            </Grid>
            <Grid item>
                <span>Value:</span><br></br>
                <input id="new_value_value"></input>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={() => addVariable(variables, setVariables)} style={{padding:"8px", height:"100%"}}>
                    Add
                </Button>
            </Grid>
        </Grid>
        <TableContainer>
            <Table sx={{ minWidth: 400 }} aria-label="simple table">
                <VariableList variables={variables}/>
            </Table>
        </TableContainer>
        </DialogContent>
        </Dialog>

        <Dialog open={openRemove} onClose={handleToCloseRemove}>
        <DialogTitle>
            {"remove variable"}
        </DialogTitle>
        <DialogContent>
        <Grid container direction="row" spacing={1.5} padding={0.5} maxHeight={"10em"}>
            <Grid item>
                <span style={{fontSize: "12px"}}>Name: </span><br></br>
                <input id="variable_to_remove_name"></input> <br></br>
            </Grid>
            <Grid item>
                <Button variant="outlined" onClick={() => removeVariable(variables, setVariables)} style={{padding:"8px", height:"100%"}}>
                    Remove
                </Button>
            </Grid>
        </Grid>
        <TableContainer>
            <Table sx={{ minWidth: 400 }} aria-label="simple table">
                <VariableList variables={variables}/>
            </Table>
        </TableContainer>
        </DialogContent>
        </Dialog>
        </> 
        );
}

export default Variables;

function VariableList ({variables}) {
    return (
        CreateVariableRows(variables)
    )
 }

 const CreateVariableRows = function (variables) {
    var rows = []
    for(var k of Object.keys(variables)){
        rows.push(
            <TableRow>
                <TableCell style={{fontSize: "12px"}}>{k}</TableCell>
                <TableCell style={{fontSize: "12px"}}>{variables[k]}</TableCell>
            </TableRow>);  
    }
    return rows;
 }

 function removeVariable(variables, setVariables){
    var name = document.getElementById("variable_to_remove_name").value;
    var var_found = false;

    const new_variables = {};

    for(var k of Object.keys(variables)){
        if(k !== name)
        {
            new_variables[k] = variables[k];
        }else{
            var_found = true;
        }    
    }

    setVariables(new_variables);
    document.getElementById("variable_to_remove_name").value = "";
 }

 function addVariable(variables, setVariables){
    var name = document.getElementById("new_value_name").value;
    var value = document.getElementById("new_value_value").value;
    
    //convert value to number if it is one
    if(!isNaN(value)){
        value = Number.parseFloat(value);
    }

    //convert value to boolean if it is one
    if(value === "true"){
        value = true;
    }else if (value === "false"){
        value = false;
    }

    const new_variables = {};

    for(var k of Object.keys(variables)){
        new_variables[k] = variables[k];
    }
    
    new_variables[name] = value;

    setVariables(new_variables);
 }