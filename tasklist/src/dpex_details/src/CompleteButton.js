import React, { useState } from "react";


import { Button, Grid } from "@mui/material";
import Variables from "../utils/Variables";

function CompleteButton() {
  const [error_status, setErrorStatus] = useState("");
  const [variables, setVariables] = useState({});

  return (<>
    <Grid container direction="column" spacing={2}>
      <Grid container direction="row" spacing={1} padding={1}>
        Process Variables:<br></br>
      </Grid>
    </Grid>
    <Variables variables={variables} setVariables={setVariables}/><br></br>
    <Button variant="outlined"  onClick={() => complete(setErrorStatus, variables)} style={{marginTop:"10px"}}>Complete</Button><br></br>
    <span>{error_status}</span>
  </>);
}

export default CompleteButton;

async function complete(setErrorStatus, variables){
    var url = "http://localhost:8086/api/camundaComplete";
    const heads = {
      "Content-type": "application/json"
    }

    var body = {
      "lir": window.my_camunda_task["task"]["processInstanceId"],
      //"task": window.my_camunda_task["task"]["name"],
      "task": window.my_camunda_task["task"]["name"],
      "taskLifeCycleStage": "COMPLETE",
      "bpmId": getBpmId(),
      "processVariables": JSON.stringify(variables)
    };

    const request = new XMLHttpRequest();

    request.open("POST", url, true);
    request.setRequestHeader("Content-Type", "application/json");
    request.onreadystatechange = () => {
        if (request.readyState == 4) {
            if(request.status < 300 && request.status > 199){
                setErrorStatus("Success!");
            }else{
              setErrorStatus("Error: " + request.status);
            }
        }
    };
    setErrorStatus("Loading...")
    request.send(JSON.stringify(body));
  }

  function getBpmId(){
    return document.bpmId;
  }