import React from "react";
import ReactDOM from "react-dom";

import CompleteButton from "./CompleteButton.js";
import ClaimButton from "./ClaimButton.js";

let container;

export default {
    id: 'tasklist.dpex.claim',
    pluginPoint: 'tasklist.task.detail',
    priority: 9001,
    render: (node, {api, processDefinitionId}, task) => {
      container = node;
      window.my_camunda_api = api;
      window.my_camunda_task = task;
      if(!task.task["assignee"]){
        ReactDOM.render(
          <ClaimButton/>,
          container
        );
      }else{
        ReactDOM.render(
          <CompleteButton/>,
          container
        );
      }
    },
    properties: {
      label: 'dpex'
    }
  
  }